package com.iodigital.assignment.repository;

import com.iodigital.assignment.domain.TedTalk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface TedTalkRepository extends JpaRepository<TedTalk, Long>, JpaSpecificationExecutor<TedTalk> {
}
