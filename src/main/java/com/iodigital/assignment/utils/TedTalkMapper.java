package com.iodigital.assignment.utils;

import com.iodigital.assignment.domain.TedTalk;
import com.iodigital.assignment.dto.TedTalkDTO;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TedTalkMapper {

    private static final SimpleDateFormat formatter = new SimpleDateFormat("MMMM yyyy", Locale.ENGLISH);

    public static TedTalk map(TedTalkDTO tedTalkDTO) {

        return TedTalk.builder()
                .title(tedTalkDTO.getTitle())
                .author(tedTalkDTO.getAuthor())
                .date(getDate(tedTalkDTO))
                .views(getViews(tedTalkDTO))
                .likes(getLikes(tedTalkDTO))
                .link(tedTalkDTO.getLink())
                .build();
    }

    private static Date getDate(TedTalkDTO tedTalkDTO) {

        try {
            return formatter.parse(tedTalkDTO.getDate());
        } catch (ParseException e) {
            log.error("the date is not well formatted: {}", tedTalkDTO);
            return null;
        }
    }

    private static Long getViews(TedTalkDTO tedTalkDTO) {

        try {
            return Long.parseLong(tedTalkDTO.getViews());
        } catch (NumberFormatException e) {
            log.error("the views is not well formatted: {}", tedTalkDTO);
            return Long.MIN_VALUE;
        }
    }

    private static Long getLikes(TedTalkDTO tedTalkDTO) {

        try {
            return Long.parseLong(tedTalkDTO.getLikes());
        } catch (NumberFormatException e) {
            log.error("the likes is not well formatted: {}", tedTalkDTO);
            return Long.MIN_VALUE;
        }
    }
}
