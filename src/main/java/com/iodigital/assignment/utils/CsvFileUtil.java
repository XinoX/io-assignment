package com.iodigital.assignment.utils;

import com.iodigital.assignment.dto.TedTalkDTO;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

;


@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CsvFileUtil {

    public static List<TedTalkDTO> normalize(String filePath) throws FileNotFoundException {
        log.debug("reading ted talks from the file: {}", filePath);
        return new CsvToBeanBuilder<TedTalkDTO>(new FileReader(filePath)).withType(TedTalkDTO.class).withSkipLines(1).build().parse();
    }


}
