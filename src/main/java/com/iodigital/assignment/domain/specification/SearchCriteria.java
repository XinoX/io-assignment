package com.iodigital.assignment.domain.specification;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class SearchCriteria {

    private String key;

    private String operation;

    private Object value;
}
