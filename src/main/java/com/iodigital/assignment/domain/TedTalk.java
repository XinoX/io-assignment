package com.iodigital.assignment.domain;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TED_TALK")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class TedTalk extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(length = 250, nullable = false)
    private String title;

    @NotNull
    @Column(length = 100, nullable = false)
    private String author;

    @NotNull
    @Column(nullable = false)
    private Date date;

    @NotNull
    @Column(nullable = false)
    private Long views;

    @NotNull
    @Column(nullable = false)
    private Long likes;

    @NotNull
    @Column(length = 500, nullable = false)
    private String link;

}
