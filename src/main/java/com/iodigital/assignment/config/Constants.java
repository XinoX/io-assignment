package com.iodigital.assignment.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/** Application constants. */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Constants {
  public static final String SYSTEM = "system";
}
