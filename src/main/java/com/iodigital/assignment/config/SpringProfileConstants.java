package com.iodigital.assignment.config;

public interface SpringProfileConstants {
    String SPRING_PROFILE_DEVELOPMENT = "dev";
    String SPRING_PROFILE_TEST = "test";
    String SPRING_PROFILE_ACCEPTANCE = "acc";
    String SPRING_PROFILE_PRODUCTION = "prod";
}
