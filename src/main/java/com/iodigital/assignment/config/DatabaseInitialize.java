package com.iodigital.assignment.config;

import com.iodigital.assignment.domain.TedTalk;
import com.iodigital.assignment.repository.TedTalkRepository;
import com.iodigital.assignment.utils.CsvFileUtil;
import com.iodigital.assignment.utils.TedTalkMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Configuration
@RequiredArgsConstructor
@ConditionalOnProperty(value = "application.database.initialize", havingValue = "true", matchIfMissing = true)
public class DatabaseInitialize {

    @Value("${application.database.data-file-path}")
    private String initFilePath;

    private final TedTalkRepository tedTalkRepo;

    @PostConstruct
    public void initializeDatabase() throws FileNotFoundException {

        List<TedTalk> tedTalks = CsvFileUtil.normalize(initFilePath).stream().map(TedTalkMapper::map).collect(Collectors.toList());
        log.info("read and normalized {} ted talks from the file", tedTalks.size());
        tedTalkRepo.saveAll(tedTalks);
    }
}
