package com.iodigital.assignment.resources;

import com.iodigital.assignment.domain.TedTalk;
import com.iodigital.assignment.domain.specification.SearchCriteria;
import com.iodigital.assignment.domain.specification.TedTalkSpecification;
import com.iodigital.assignment.repository.TedTalkRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/rest/v1/ted-talk/")
public class TedTalkController {

  private final TedTalkRepository tedTalkRepo;

  @PutMapping
  public ResponseEntity addNewTedTalk(@RequestBody TedTalk tedTalk) {

    tedTalk.setId(-1L);
    tedTalkRepo.save(tedTalk);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  @PostMapping
  public ResponseEntity editTedTalk(@RequestBody TedTalk tedTalk) {

    tedTalkRepo.findById(tedTalk.getId()).orElseThrow(EntityNotFoundException::new);
    tedTalkRepo.save(tedTalk);
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  @DeleteMapping("{id}")
  public ResponseEntity deleteTedTalk(@PathVariable long id) {

    tedTalkRepo.findById(id).orElseThrow(EntityNotFoundException::new);
    tedTalkRepo.deleteById(id);
    return ResponseEntity.status(HttpStatus.OK).build();
  }

  @GetMapping("/search")
  public List<TedTalk> search(
      @Nullable String author, @Nullable String title, @Nullable Long views, @Nullable Long likes) {

    if (StringUtils.isBlank(author)
        && StringUtils.isBlank(title)
        && Objects.isNull(views)
        && Objects.isNull(likes)) {
      return Collections.emptyList();
    }

    Specification<TedTalk> specification =
        Specification.where(new TedTalkSpecification(new SearchCriteria("id", ">", 0)));
    if (StringUtils.isNotBlank(author)) {
      specification =
          specification.and(new TedTalkSpecification(new SearchCriteria("author", ":", author)));
    }
    if (StringUtils.isNotBlank(title)) {
      specification =
          specification.and(new TedTalkSpecification(new SearchCriteria("title", ":", title)));
    }
    if (!Objects.isNull(views)) {
      specification =
          specification.and(new TedTalkSpecification(new SearchCriteria("views", ":", views)));
    }
    if (!Objects.isNull(likes)) {
      specification =
          specification.and(new TedTalkSpecification(new SearchCriteria("likes", ":", likes)));
    }
    return tedTalkRepo.findAll(specification);
  }
}
