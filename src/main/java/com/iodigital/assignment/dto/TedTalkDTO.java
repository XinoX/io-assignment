package com.iodigital.assignment.dto;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

@Data
public class TedTalkDTO {

    @CsvBindByPosition(position = 0)
    private String title;

    @CsvBindByPosition(position = 1)
    private String author;

    @CsvBindByPosition(position = 2)
    private String date;

    @CsvBindByPosition(position = 3)
    private String views;

    @CsvBindByPosition(position = 4)
    private String likes;

    @CsvBindByPosition(position = 5)
    private String link;
}