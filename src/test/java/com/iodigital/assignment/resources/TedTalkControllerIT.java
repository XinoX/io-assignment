package com.iodigital.assignment.resources;

import com.iodigital.assignment.domain.TedTalk;
import com.iodigital.assignment.repository.TedTalkRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.Optional;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TedTalkControllerIT {

  @Autowired private TedTalkController tedTalkController;

  @Autowired private TedTalkRepository tedTalkRepo;

  @Test
  @DisplayName("total of records should increase after adding new ted talk")
  void addNewTedTalk() {
    long countBefore = tedTalkRepo.count();
    tedTalkController.addNewTedTalk(
        TedTalk.builder()
            .author("DUMMY_AUTHOR")
            .title("DUMMY_TITLE")
            .date(new Date())
            .link("DUMMY_LINK")
            .views(Long.MIN_VALUE)
            .likes(Long.MAX_VALUE)
            .build());
    long countAfter = tedTalkRepo.count();
    Assertions.assertEquals(countBefore + 1, countAfter);
  }

  @Test
  @DisplayName("editing a ted talk")
  void editTedTalk() {
    TedTalk tedTalkBefore = tedTalkRepo.findById(1L).orElseThrow(IllegalAccessError::new);
    final String author = "KHODABAKHSH BAKHTIARI";
    tedTalkBefore.setAuthor(author);
    tedTalkController.editTedTalk(tedTalkBefore);
    TedTalk tedTalkAfter = tedTalkRepo.findById(1L).orElseThrow(IllegalAccessError::new);
    Assertions.assertEquals(author, tedTalkAfter.getAuthor());
  }

  @Test
  @DisplayName("delete a ted talk")
  void deleteTedTalk() {
    TedTalk tedTalkBefore = tedTalkRepo.findById(2L).orElseThrow(IllegalAccessError::new);
    tedTalkController.deleteTedTalk(2);
    Optional<TedTalk> tedTalkAfter = tedTalkRepo.findById(2L);
    Assertions.assertTrue(tedTalkAfter.isEmpty());
  }

  @Test
  @DisplayName("search for ted talks")
  void search() {
    tedTalkRepo.save(
        TedTalk.builder()
            .author("DUMMY_AUTHOR_FOR_SEARCH")
            .title("DUMMY_TITLE_FOR_SEARCH")
            .date(new Date())
            .link("DUMMY_LINK_FOR_SEARCH")
            .views(10L)
            .likes(15L)
            .build());

    Assertions.assertEquals(0, tedTalkController.search(null, null, null, null).size());
    Assertions.assertTrue(
        tedTalkController.search("DUMMY_AUTHOR_FOR_SEARCH", null, null, null).size() >= 1);
    Assertions.assertTrue(
        tedTalkController
                .search("DUMMY_AUTHOR_FOR_SEARCH", "DUMMY_TITLE_FOR_SEARCH", null, null)
                .size()
            >= 1);
    Assertions.assertTrue(
        tedTalkController
                .search("DUMMY_AUTHOR_FOR_SEARCH", "DUMMY_TITLE_FOR_SEARCH", 10L, null)
                .size()
            >= 1);
    Assertions.assertTrue(
        tedTalkController
                .search("DUMMY_AUTHOR_FOR_SEARCH", "DUMMY_TITLE_FOR_SEARCH", 10L, 15L)
                .size()
            >= 1);
  }
}
