package com.iodigital.assignment.config;

import com.iodigital.assignment.config.h2.H2ConfigurationHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;

import java.sql.SQLException;

import static com.iodigital.assignment.config.SpringProfileConstants.SPRING_PROFILE_DEVELOPMENT;
import static com.iodigital.assignment.config.SpringProfileConstants.SPRING_PROFILE_TEST;

@Slf4j
@Configuration
@RequiredArgsConstructor
public class DatabaseConfiguration {

  private final Environment env;

  /**
   * Open the TCP port for the H2 database, so it is available remotely.
   *
   * @return the H2 database TCP server.
   * @throws SQLException if the server failed to start.
   */
  @Bean(initMethod = "start", destroyMethod = "stop")
  @Profile({SPRING_PROFILE_DEVELOPMENT, SPRING_PROFILE_TEST})
  public Server h2TCPServer() throws SQLException {
    String port = getValidPortForH2();
    log.debug("H2 database is available on port {}", port);
    return H2ConfigurationHelper.createServer(port);
  }

  private String getValidPortForH2() {
    int port = Integer.parseInt(env.getProperty("server.port"));
    if (port < 10000) {
      port = 10000 + port;
    } else {
      if (port < 63536) {
        port = port + 2000;
      } else {
        port = port - 2000;
      }
    }
    return String.valueOf(port);
  }
}
