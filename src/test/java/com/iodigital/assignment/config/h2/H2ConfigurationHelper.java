package com.iodigital.assignment.config.h2;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;

import java.sql.SQLException;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class H2ConfigurationHelper {

  public static Server createServer() throws SQLException {
    return createServer("9092");
  }

  public static Server createServer(String port) throws SQLException {
    return Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", port);
  }
}
