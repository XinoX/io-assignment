# IO Assignment

Hi! Appolgies in advance for consice documentation!!

# Prerequisite
Add the following entry to your hosts file(e.g. `/etc/hosts`):
> 127.0.0.1 keycloak

# How to build?
Having java 18 and maven at hand, you can build a container from the application with the help of following command:

> mvn compile jib:dockerBuild -Pprod

# How to run?
change directory to: `src/main/docker` and run the following command:

> docker compose up

Give it about a minute to load.

# How to test the application?

1. Create a new user for the application([helper link](https://www.appsdeveloperblog.com/keycloak-creating-a-new-user/)). Go to this URL: [http://localhost:9080/auth/](http://localhost:9080/auth/) and enter using the following credentials:
   **Username**: admin
   **Password**: admin
2. Open this URL: [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html) and enter with created credentials from step 1.
3. Try the endpoints with this OpenAPI user interface.
